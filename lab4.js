function eje1(){
	let n = prompt("Dime un número");
	let res = "<table class='table table-bordered' id='tabla'><thead><h5>Cuadrados y cubos</h5></thead><tbody><tr><th>Numero</th><th>Cuadrado</th><th>Cubo</th></tr>"
	for(let i = 1; i <= n; i++){
		res += "<tr>";
		res += "<td>" + i + "</td>";
		res += "<td>" + i*i + "</td>";
		res += "<td>" + i*i*i + "</td>";
		res += "</tr>";
	}
	res += "</tbody></table>";
	document.getElementById("eje1").innerHTML = res;
}
function eje2(){
	let ini = new Date().getTime();
	let n1 = Math.random();
	let n2 = Math.random();
	n1 *= 10;
	n2 *= 10;
	n1 = Math.floor(n1);
	n2 = Math.floor(n2);
	let res = n1*n2;
	let pregunta = "¿Cuánto es " + n1 + " multiplicado por " + n2 + "?";
	let dado = prompt(pregunta);
	let fin = new Date().getTime();
	dado = dado * 1;
	let timer = (fin - ini)/1000;
	let mensaje = "<header><h5>¡Pop Quiz!</h5></header><p>La pregunta fue: ";
	mensaje += pregunta;
	mensaje += "</p><p>Tu respuesta, dada en " + timer + " segundos, fue: " + dado + "</p>";
	mensaje += "<p>Y tu respuesta fue... [drumroll please]</p>"
	if(res == dado){
		mensaje += "<h3>¡Correcta! ¡Felicidades!</h3>"
	} else{
		mensaje += "<h6>incorrecta... no te preocupes, aún puedes regresar a la primaria a estudiar esas tablas de multiplicación<h6>"
	}
	document.getElementById("eje2").innerHTML = mensaje;

}

function eje3(){
	let n = prompt("Dame un número entre 1 y 10");
	n *= 1;
	let mensaje = "";
	if(n < 1 || n > 11){
		mensaje += "<h6>Por qué me harías ésto...</h6>";
		document.getElementById("eje3").innerHTML = mensaje;
		return -1;
	}
	mensaje = "<header><h5>Un cuenta cuentos.... o bueno...</h5></header>";
	let j;
	let mens;
	let neg = 0;
	let cero = 0;
	let pos = 0;
	mensaje += "<table class='table table-bordered' id='tabla'><tbody><tr><th colspan = '" + n + "'>Arreglo</th></tr><tr>";
	for(let i = 1; i <= n; i++){
		mens = "Dame el " + i + " elemento:";
		j = prompt(mens);
		j *= 1;
		mensaje += "<td>" + j + "</td>";
		if(j < 0){
			neg++;
		} else if (j == 0){
			cero ++;
		} else if(j > 0){
			pos++;
		}
	}
	mensaje += "</tr></tbody></table><br><p><strong>Números negativos: </strong> " + neg + "</p>";
	mensaje += "<p><strong>Ceros: </strong> " + cero + "</p>";
	mensaje += "<p><strong>Números positivos: </strong> " + pos + "</p>";
	document.getElementById("eje3").innerHTML = mensaje;
}
function eje4(){
	let n = prompt("Ingrese la cantidad de arreglos a sacar promedio: ");
	n *= 1;
	let mensaje = "<header><h5>Ser Promedio :) </h5></<header>";
	if(n > 5){
		mensaje += "<h6>Me rehuso a hacer más de 5 arreglos...</h6>";
		document.getElementById("eje4").innerHTML = mensaje;
		return -1;
	}
		mensaje += "<table class='table table-bordered' id='tabla'><tbody><tr><th colspan='10'>Arreglo</th><th>Promedio</th></tr>";
	for(let i = 1; i <= n; i++){
		let m = prompt("Ingrese la cantidad de elementos del " + i + "  arreglo menor o igual a 11: ");
		m *= 1;
		if(m > 10){
			mensaje += "<tr><td colspan='11'>FAVOR DE SEGUIR LAS INSTRUCCIONES :C</td></tr>";
		}else{
			mensaje += "<tr>";
			let k;
			let mens;
			let sum = 0;
			let cant = 0;
			for(let j = 1; j <= m; j++){
				mens = "Ingrese el " + j + " elemento del arreglo " + i + ": ";
				k = prompt(mens);
				k *= 1;
				if (!isNaN(k)){
					sum += k;
					cant++;
				}
				mensaje += "<td>" + k + "</td>";
			}
			mensaje += "<td id='left' colspan='" + (11 - m) + "'>" + (sum/cant) + "</td></tr>";
		}
	}
	document.getElementById("eje4").innerHTML = mensaje;
}
function eje5(){
	let n = prompt("Ingrese un número a invertir: ");
	n += "";
	let mensaje = "<header><h5>Slicing slices</h5></header>";
	mensaje += "<p><strong>Original: </strong>" + n + "</p>";
	let l = n.length;
	let dig = n.slice(-1);
	mensaje += "<p><strong>Inverso: </strong>" + dig;
	for(let i = 2; i <= l; i++){
		dig = n.slice(-i, (-i)+1);
		mensaje += dig;
	}
	mensaje += "</p>";
	document.getElementById("eje5").innerHTML = mensaje;
}
function eje6(){
    document.getElementById("eje6").innerHTML = '<header><h5>DADO!</h5></header><h6>Haz clic para tirar el dado.</h6> <button onclick="myFunction()">Tira dado</button> <p id="dado"></p>';
}
function myFunction() {
  var num = Math.random();
  num = num * 6;
  num = Math.floor(num) + 1;
  document.getElementById("dado").innerHTML = num;
}
